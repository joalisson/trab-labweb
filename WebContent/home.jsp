<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%> 

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8">
  <meta charset="utf-8">
  <title>Monitoria Online</title>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/bootstrap.css" rel="stylesheet">
  <link href="css/font-awesome.min.css" rel="stylesheet">
  <link href="css/font-awesome.css" rel="stylesheet">
  <link href="css/styles.css" rel="stylesheet">
</head>
<body>

  <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
    <div class="container">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">P�gina Inicial</a>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <h3></i>Listar</h3>
        <hr>
        <ul class="nav nav-stacked">

          <li>
           <div class="btn-group btn-group-justified" role="group" aria-label="...">
            <div class="btn-group" role="group">
              <button type="button" class="btn btn-default esp"><span class="glyphicon glyphicon-book"></span> Disciplina</button>
            </div>
          </div>
        </li>
          <li>
            <div class="btn-group btn-group-justified" role="group" aria-label="...">
              <div class="btn-group" role="group">
                <button type="button" class="btn btn-default esp"><span class="glyphicon glyphicon-calendar"></span> Data</button>
              </div>
            </div>
          </li>
          <li>  
            <div class="btn-group btn-group-justified" role="group" aria-label="...">
              <div class="btn-group" role="group">
                <button type="button" class="btn btn-default esp"><span class="glyphicon glyphicon-time"></span> Data</button>
              </div>
            </div>
          </li>

        <li>
         <div class="btn-group btn-group-justified" role="group" aria-label="...">
          <div class="btn-group" role="group">
            <button type="button" class="btn btn-default">
              <span class="glyphicon glyphicon-map-marker"></span> Local
            </button>
          </div>
        </div>
      </li>
    </ul>
    <hr>
  </div>

  <div class="col-sm-9">
   <h3 class="center">Monitorias Dispon�veis</h3>  
   <hr>
   <div class="row">
    <div class="col-md-20">
      <div class="panel panel-default">
        <div class="panel-heading"><h4>Todas</h4></div>
        <div class="panel-body">
            <div class="list-group">
              <a href="salaMonitoria.jsp" class="list-group-item active">
                <h4 class="list-group-item-heading">C�lculo A</h4>
                <p class="list-group-item-text"><b>Assunto:</b> Limites e Derivadas 
                  <br><b>Hor�rio:</b> 12:00 as 13:00</p></br> 
                  <button type="button" class="btn btn-default">Entrar</button></p>
                </a>
              </div>
              <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h4 class="list-group-item-heading">Hist�ria</h4>
                  <p class="list-group-item-text">Assunto: 1� Guerra Mundial</p></br>
                  <button type="button" class="btn btn-default">Entrar</button></p>
                </a>
              </div>
              <div class="list-group">
                <a href="#" class="list-group-item active">
                  <h4 class="list-group-item-heading">L�gica de Programa��o</h4>
                  <p class="list-group-item-text"><b>Assunto:</b> Operadores l�gicos
                    <br><b>Hor�rio:</b> 10:00 as 11:00</p></br>  
                    <button type="button" class="btn btn-default">Entrar</button></p>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>              
    </div>

    <footer class="text-center"><strong>Monitoria Online</strong> - Todos os direitos reservados - 2015</a></footer>

    <!-- script references -->
    <script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
  </html>