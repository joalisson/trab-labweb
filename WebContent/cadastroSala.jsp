<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Laboratório Web</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style-cadastro.css">
    <metaname="viewport"content="width=device-width, initial-scale=1">
</head>
<body>

    <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header"></div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-8 col-xs-offset-2 content">
                <h1>Cadastro de Sala</h1> <hr>
            </div>
            <div class="col-md-4 col-md-offset-4 col-xs-12 col-sm-6 col-sm-offset-3">
                <form action="" method="post">
                    <div class="form-group">
                        <label for="selectDisciplina">Disciplina:</label>
                        <select name="Selectdisciplina" class="form-control">
                            <option value="select">Selecione</option>
                            <option value="Geometria Analitica">Geometria Analítica</option>
                            <option value="Engenharia de Software">Engenharia de Software</option>
                            <option value="Laboratorio Web">Laboratório de Programação Web</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="inputAssunto">Asssunto</label>
                        <input type="text" class="form-control" name="assunto" required>
                    </div>
                    
                    <div class="form-group">
                        <label for="inputSenha">Senha:</label>
                        <input type="password" class="form-control" name="senha" required>
                    </div>
                    <div class="form-group">
                        <label for="inputHorario">Horário:</label>
                        <input type="datetime" class="form-control" name="horario" required>
                    </div>
                    <div class="form-group">
                        <label for="inputSenha">Repetir Senha:</label>
                        <input type="password" class="form-control" name="confirm-senha" required>
                    </div>
                    <div class="col-md-12 btn-cadastro">
                        <div class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2">
                            <button type="submit" class="btn btn-default btn-block">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
</body>
</html> 