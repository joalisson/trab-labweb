<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Trabalho de Laboratório Web</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style-cadastro.css">
    <metaname="viewport"content="width=device-width, initial-scale=1">
	<title>Monitoria Online</title>
</head>
<body>
	<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header"></div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-8 col-xs-offset-2 content">
                <h1>Cadastro</h1> <hr>
            </div>
            <div class="col-md-4 col-md-offset-4 col-xs-12 col-sm-6 col-sm-offset-3">
                <form action="RecebeUsuario" method="post">
                    <div class="form-group">
                        <label for="inputNome">Nome:</label>
                        <input type="text" class="form-control" name="nome" required>
                    </div>
                    <div class="form-group">
                        <label for="inputSobrenome">Sobrenome:</label>
                        <input type="text" class="form-control" name="sobrenome" required>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail">Email:</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="inputSenha">Senha:</label>
                        <input type="password" class="form-control" name="senha" required>
                    </div>
                    <div class="form-group">
                        <label for="inputSenha">Repetir Senha:</label>
                        <input type="password" class="form-control" name="confirm-senha" required>
                    </div>
                    <div class="col-md-12 btn-cadastro">
                        <div class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2">
                            <button type="submit" class="btn btn-default btn-block">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html> 