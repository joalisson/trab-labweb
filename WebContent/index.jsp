<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Trabalho de Laboratório Web</title>
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style-index.css">
    <metaname="viewport"content="width=device-width, initial-scale=1">
</head>
<body>

    <div id="top-nav" class="navbar navbar-inverse navbar-static-top">
        <div class="container">
            <div class="navbar-header"></div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-xs-8 col-xs-offset-2 content">
                <h1>Monitoria</h1> <hr>
            </div>
            <div class="col-md-4 col-md-offset-4 col-xs-12 col-sm-6 col-sm-offset-3 form-login">
                <form action="" method="post">
                    <div class="form-group">
                        <input type="email" class="form-control" name="email" placeholder="E-mail" required>
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="senha" placeholder="Senha" required>
                    </div>
                    <div class="col-md-8 col-md-offset-2 btn-login">
                        <button type="submit" class="btn btn-default btn-block">Submit</button><br>
                        <a href="cadastro.jsp">Cadastre-se</a>
                    </div>
                </form>
            </div>
        </div>
    </div>

</div>
</body>
</html>