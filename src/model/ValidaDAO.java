package model;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class ValidaDAO {
	
	
	public static Usuario ValidaUsuario(String email, String senha) throws SQLException, ClassNotFoundException{
	
		Connection conexao = new ConnectionFactory().getConnection();
		String sql = "SELECT * FROM users  WHERE (email = ?) AND (senha = ?)";
		PreparedStatement stmt = null;
		try {
			
			stmt = (PreparedStatement) conexao.prepareStatement(sql);
			stmt.setString(1, email);
			stmt.setString(2, senha);
			stmt.execute();
			ResultSet resultSet = stmt.executeQuery();
			
			Usuario user = new Usuario();
			
			user.setId(resultSet.getInt("id"));
			user.setNome(resultSet.getString("nome"));
			user.setSobrenome(resultSet.getString("sobrenome"));
			user.setEmail(resultSet.getString("email"));
			user.setSenha(resultSet.getString("senha"));
			
			return user;
		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		}		
	}
}
