package model;

import java.sql.Connection;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class UsuarioDAO {
	
	
	public static void insereUsuario(Usuario form) throws SQLException, ClassNotFoundException{
	
		Connection conexao = new ConnectionFactory().getConnection();
		String sql = "INSERT INTO users (nome, sobrenome, email, senha) VALUES (?,?,?,?)";
		PreparedStatement stmt = null;
		try {
			stmt = (PreparedStatement) conexao.prepareStatement(sql);
			stmt.setString(1, form.getNome());
			stmt.setString(2, form.getSobrenome());
			stmt.setString(3, form.getEmail());
			stmt.setString(4, form.getSenha());
			stmt.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}	
		stmt.close();
		conexao.close();
	}
}
