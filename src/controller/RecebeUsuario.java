package controller;
import model.Usuario;
import model.UsuarioDAO;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/RecebeUsuario")
public class RecebeUsuario extends HttpServlet {
	private static final long serialVersionUID = 1L;
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nome 		= request.getParameter("nome");
		String sobrenome 	= request.getParameter("sobrenome");
		String email 		= request.getParameter("email");
		String senha 		= request.getParameter("senha");
		
		Usuario user = new Usuario();
		
		user.setNome(nome);
		user.setEmail(email);
		user.setSobrenome(sobrenome);
		user.setSenha(senha);
		
		try {
			UsuarioDAO.insereUsuario(user);
		} catch (SQLException e) {

			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		response.sendRedirect("home.jsp");
	}
}
